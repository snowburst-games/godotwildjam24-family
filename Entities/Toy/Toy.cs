using Godot;
using System;

public class Toy : Control, IInventoriable
{

	private InventoryItem _inventoryItem;
	public InventoryItem InventoryItem
	{
		get
		{
			return _inventoryItem;
		}
		set
		{
			_inventoryItem = value;
		}
	}
	public void Init(InventoryItem inventoryItem)
	{
		this.InventoryItem = inventoryItem;
		this.InventoryItem.Cost = 150;
	}

	public event InventoryItem.ItemUsedDelegate ItemUsed;

	public delegate void ToyUsedDelegate(int itemID);
	public event ToyUsedDelegate ToyUsed;

	private string _itemName = "Toy";
	public string ItemName
	{
		get
		{
			return _itemName;
		}
		set
		{
			_itemName = value;
		}
	}

	private float _health = 100;
	public float Health
	{
		get
		{
			return _health;
		}
		set
		{
			_health = value;
		}
	}
	public void Use()
	{
		ItemUsed?.Invoke(InventoryItem.ItemID);
		ToyUsed?.Invoke(InventoryItem.ItemID);
	}

}
