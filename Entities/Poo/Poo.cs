using Godot;
using System;

public class Poo : Control
{
	// to dynamically adjust the timerfumes waittime according to the current speed
	// have a _currentSpeed var and a _previousSpeed var
	// OnSpeedChanged(int speed)
	// set _currentSpeed to _previousSpeed (which starts off as 1)
	// _currentSpeed = speed
	// _newStartTime = TimerFumes.TimeLeft * _previousSpeed
	// _newStartTime /= _currentSpeed
	// TimerFumes.WaitTime *= _previousSpeed
	// TimerFumes.WaitTime /= _currentSpeed
	// TimerFumes.Start(_newStartTime)
	// can do similar for fox
	
	public delegate void MadeFumesDelegate();
	public event MadeFumesDelegate MadeFumes;

	public delegate void PooClickedDelegate(Vector2 pos);
	public event PooClickedDelegate PooClicked;
	
	private int _currentMultiplier = 10;

	public override void _Ready()
	{
		SetRandomWaitTime();
	}

	private void SetRandomWaitTime()
	{
		GetNode<Timer>("TimerFumes").WaitTime = new Random().Next(120,800);
	}

	public void DivideTimerFumesWaitTime(int multiplier)
	{
		_currentMultiplier = multiplier;
		GetNode<Timer>("TimerFumes").WaitTime /= multiplier;
	}

	public void Start()
	{
		GetNode<Timer>("TimerFumes").Start();
	}

	public void TimerFumesPaused(bool pause)
	{
		GetNode<Timer>("TimerFumes").Paused = pause;
	}
	
	private void OnTimerFumesTimeout()
	{
		MadeFumes?.Invoke();
		SetRandomWaitTime();
		DivideTimerFumesWaitTime(_currentMultiplier);
		GetNode<Timer>("TimerFumes").Start();
	}

	public Vector2 GetPooSize()
	{
		TextureRect rect = GetNode<TextureRect>("TextureRect");
		return new Vector2(rect.RectSize.x * rect.RectScale.x, rect.RectSize.y * rect.RectScale.y);
	}

	private void OnPooGUIInput(InputEvent e)
	{
		if (e.IsPressed())
		{
			if (e is InputEventMouseButton btn)
			{
				if (btn.ButtonIndex == (int)ButtonList.Left)
				{
					PooClicked?.Invoke(this.RectPosition);
					Die();
				}
			}
		}
	}

	// public override void _Process(float delta)
	// {
	// 	// GD.Print(GetNode<Timer>("TimerFumes").TimeLeft);
	// }

	private void Die()
	{
		QueueFree();
	}

}

// if (event.is_pressed() and event.button_index == BUTTON_LEFT):
//     print("Wow, a left mouse click")
