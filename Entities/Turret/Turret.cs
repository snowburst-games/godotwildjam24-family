using Godot;
using System;

public class Turret : Control, IInventoriable
{

	private InventoryItem _inventoryItem;
	public InventoryItem InventoryItem
	{
		get
		{
			return _inventoryItem;
		}
		set
		{
			_inventoryItem = value;
		}
	}
	public void Init(InventoryItem inventoryItem)
	{
		this.InventoryItem = inventoryItem;
		this.InventoryItem.Cost = 275;
	}

	public event InventoryItem.ItemUsedDelegate ItemUsed;

	public delegate void TurretUsedDelegate(int itemID);
	public event TurretUsedDelegate TurretUsed;

	private string _itemName = "Defence Tower";
	
	public string ItemName
	{
		get
		{
			return _itemName;
		}
		set
		{
			_itemName = value;
		}
	}
	private float _health = 100;
	public float Health
	{
		get
		{
			return _health;
		}
		set
		{
			_health = value;
		}
	}

	public void Use()
	{
		ItemUsed?.Invoke(InventoryItem.ItemID);
		TurretUsed?.Invoke(InventoryItem.ItemID);
	}

}
