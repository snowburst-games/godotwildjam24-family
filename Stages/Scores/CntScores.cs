// CntScores: responsible for Loading scores from file, and Rendering scores into a VBoxContainer
// Intended to be used in conjunction with a ScoreHandler script which handles saving scores to file
using Godot;
using System;
using System.Collections.Generic;

public class CntScores : VBoxContainer
{
    private int maxLines = 17;

    private System.Collections.Generic.List<ScoresUnit> scoreList = new System.Collections.Generic.List<ScoresUnit>();

    private DynamicFont fontScore;

    public int gameMode {set; get;} = 1;

    public override void _Ready()
    {        
        // Set the font we are going to use for the score labels
        fontScore = (DynamicFont) GD.Load("res://Common/UI/Fonts/FntMenuSmall.tres");
        
        // The amount of seperation (vertically) between scores
        Set("custom_constants/separation", 40);

        // // Make a jagged array to store 4 score text elements per line of score
        // for (int i = 0; i < scoreList.Count; i++)
        // {
        //     scoreList[i] = new string[4];
        // }
    }

    public void ShowScores()
    {
        LoadScores();
        // TestScores();
        ClearRenderedScores();
        RenderScores();
    }

    private void ClearRenderedScores()
    {
        foreach (Node child in GetChildren())
            child.QueueFree();
    }

    private void RenderScores()
    {
       // Now we are managing the position to place labels (only need to focus on horizontal, as VBox manages vertical)
        // We do not use HBox as it does not give the separation we want
        // We track a position variable and increment the horizontal position depending on the element, to keep all aligned
        Vector2 position = new Vector2(0, 0);
        float[] hPosIncrementArr = new float[6] {170,105,270,-270,270,0};


        if (gameMode == 1)
        {   // separation is different if showing single player scores
            hPosIncrementArr = new float[6] {170,105,270,0,0,0};

        }

        // limit the number of lines to the maximum displayable
        double totalLines = Math.Min(maxLines, scoreList.Count);
        if (gameMode == 2)
        {
            totalLines = Math.Min(Math.Floor(maxLines/2.0),scoreList.Count); // if multiplayer, can show half the number of lines
        }

        for (int i = 0; i < totalLines; i++)
        {
            Container box = new Container();
            AddChild(box);

            Container box2 = new Container();

            if (gameMode == 2)
            {
                AddChild(box2);
            }

            // Convert the scores into an array of strings to parse more easily
            string[] currentScores = new string[6] {
                scoreList[i].Date,
                scoreList[i].Level.ToString(),
                scoreList[i].PlayerName1,
                scoreList[i].Score1.ToString(),
                scoreList[i].PlayerName2,
                scoreList[i].Score2.ToString()
            };

            for (int j = 0; j < currentScores.Length; j++)
            {
                // if showing single player scores no need to do anything after the 4th element (0/1/2/3)
                if (gameMode == 1)
                {
                    if (j >= 4)
                    {
                        break;
                    }
                }


                if (currentScores[j] == null)
                    break;
                Label l = new Label();
                l.AddFontOverride("font", fontScore);
                l.Text = currentScores[j];
                l.SetPosition(position);
                if (j >= 4)
                {
                    box2.AddChild(l);
                }
                else
                {             
                    box.AddChild(l);
                }
                position.x += hPosIncrementArr[j];
            }
            position.x = 0;
        }
    }

    private void LoadScores()
    {

        string path = "PlayerData/Scores/Scores" + gameMode + ".wpd";
        // GD.Print(path);
        if (! System.IO.File.Exists(path))
        {
            GD.Print("Path does not exist!");
            return;
        }
        DataBinary scoreData = FileBinary.LoadFromFile(path);
        scoreList = (List<ScoresUnit>)scoreData.Data["scoreList"];

        // ConsoleTest();
    }

    private void ConsoleTest()
    {
        foreach (ScoresUnit unit in scoreList)
        {
            GD.Print(unit.Date);
            GD.Print(unit.Level);
            GD.Print(unit.PlayerName1);
            GD.Print(unit.Score1);
            GD.Print(unit.PlayerName2);
            GD.Print(unit.Score2);

        }
    }

    // // For testing
    private void TestScores()
    {
        scoreList.Clear();

        Random rand = new Random();

        for (int i = 0; i < maxLines; i++)
        {
            scoreList.Add(new ScoresUnit()
            {
                Date = "14/06/2014",
                Level = rand.Next(20),
                PlayerName1 = "Rand player",
                Score1 = rand.Next(1000),
                PlayerName2 = "another rand one",
                Score2 = rand.Next(500)
            });
        }

		scoreList.Sort((x,y) => (y.Score1 + y.Score2).CompareTo((x.Score1 + x.Score2)));
        // it won't show in order for single player cuz the scores are added

    }

}

[Serializable()]
public class ScoresUnit
{
    public string Date {get; set;}
    public int Level {get; set;}
    public string PlayerName1 {get; set;}
    public int Score1 {get; set;}
    public string PlayerName2 {get; set;} = "";
    public int Score2 {get; set;} = 0;
} // Date, player name, level reached, score, (if applicable) player 2 name, (if applicable) player 2 score
			// {DateTime.Now.ToString("d/M/yyyy"),
			// _levelGenerator.Level.ToString(),
			// GameSettings.Instance.PlayerNames[1],
			// _finalScores[0].ToString(),
			// "",
			// ""};	