// StageScores: simple script attached to StageScores node to display the scores
using Godot;
using System;
using System.Linq;

public class StageScores : Stage
{
	private CntScores cntScores;
	private Label lblTitleMode;
	private Node gameModeBtns;

	public override void _Ready()
	{
		base._Ready();
		cntScores = (CntScores) GetNode("CntScores");
		lblTitleMode = (Label)GetNode("LblTitleMode");
		gameModeBtns = (Node)GetNode("GameModeBtns");
		ShowSinglePlayerScores();
		//_on_BtnVsAI_pressed();
		//_on_BtnAI_pressed();
	}

	// private void _on_BtnMainMenu_pressed()
	// {
	//     ((SceneManager)GetNode("/root/SceneManager")).SimpleChangeScene(SceneData.GameScene.Menu);
	// }

	private void DisableSingleButton(TextureButton buttonToDisable)
	{
		

	   foreach (Node button in gameModeBtns.GetChildren())
		{
			if (!(button is TextureButton))
				continue;
			if (button == buttonToDisable)
				((TextureButton)button).SetDisabled(true);
			else
				((TextureButton)button).SetDisabled(false);	
		}
	}

	private void ShowSinglePlayerScores()
	{
		cntScores.gameMode = 1;
		lblTitleMode.SetText("One Player");
		cntScores.ShowScores();

		foreach (Node n in gameModeBtns.GetChildren())
		{
			if (n.Name == "BtnOnePlayer")
				DisableSingleButton((TextureButton)n);
		}


		GetNode<Label>("ScoreLbls/LblName").Text = "Name";
		GetNode<Label>("ScoreLbls/LblScore").Text = "Score";

		// DisableSingleButton((TextureButton)gameModeBtns.GetChildren().FirstOrDefault(x => ((Node)x).Name == "BtnVsAI"));
	}

	private void ShowMultiplayerScores()
	{
		cntScores.gameMode = 2;
		lblTitleMode.SetText("Two Players");
		cntScores.ShowScores();

		foreach (Node n in gameModeBtns.GetChildren())
		{
			if (n.Name == "BtnTwoPlayers")
				DisableSingleButton((TextureButton)n);
		}
		GetNode<Label>("ScoreLbls/LblName").Text = "Names";
		GetNode<Label>("ScoreLbls/LblScore").Text = "Scores";
	}

	// private void _on_BtnLocal_pressed()
	// {
	// 	ShowLocalScores();
	// }

	// private void _on_BtnAI_pressed()
	// {
	// 	ShowAIScores();
	// }

	private void _on_BtnMainMenu_pressed()
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.MainMenu);
	}


	private void OnBtnOnePlayerPressed()
	{
		ShowSinglePlayerScores();
	}


	private void OnBtnTwoPlayersPressed()
	{
		ShowMultiplayerScores();
	}
}
