using Godot;
using System;

public class TimeHandler : Node
{

	public delegate void NewDayDelegate(string day);
	public event NewDayDelegate NewDay;

	public delegate void NewHourDelegate(int hour);
	public event NewHourDelegate NewHour;

	private DateTime _gameDateTime = new DateTime(2020, 8, 15, 09, 00, 0); //DateTime dateTime = new DateTime(2016, 7, 15, 3, 15, 0);

	public delegate void SpeedChangedDelegate(int speed);
	public event SpeedChangedDelegate SpeedChanged;

	private int _speed = 10;

	public enum SpeedSetting {Paused, Normal, Double, Quadruple}

	public void SetSpeed(SpeedSetting setting)
	{
		switch (setting)
		{
			case SpeedSetting.Paused:
				_speed = 0;
				break;
			case SpeedSetting.Normal:
				_speed = 10;
				break;
			case SpeedSetting.Double:
				_speed = 20;
				break;
			case SpeedSetting.Quadruple:
				_speed = 40;
				break;
		}

		SpeedChanged?.Invoke(_speed);
	}

	public int GetSpeed()
	{
		return _speed;
	}

	public override void _Ready()
	{

	}

	public string GetTime()
	{
		// GD.Print(_gameDateTime.TimeOfDay);
		return string.Format("{0:h:mm tt}", _gameDateTime); // :ss to show seconds
	}

	public int GetHour()
	{
		return _gameDateTime.Hour;
	}

	private string _previousDay = "";
	private int _previousHour = -1;

	public string GetDay()
	{
		return string.Format("{0:dddd}", _gameDateTime);
	}

	public override void _PhysicsProcess(float delta)
	{
		_gameDateTime = _gameDateTime.AddMinutes(_speed *delta);

		if (GetDay() != _previousDay)
		{
			NewDay?.Invoke(GetDay());
			_previousDay = GetDay();
		}

		if (GetHour() != _previousHour)
		{
			NewHour?.Invoke(GetHour());
			_previousHour = GetHour();
		}
	}

}
