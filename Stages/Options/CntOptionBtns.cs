// CntOptionsBtns: Simple script that allows for disabling a single button in a node's children (keeping the others enabled)
// We can move this to utils or make it a node extension

using Godot;
using System;

public class CntOptionBtns : Node
{
    public void DisableSingleButton(Button disableBtn)
    {
        foreach (Node n in GetChildren()){
            if (n is Button){
                Button btn = (Button) n;
                if (btn != disableBtn)
                    btn.SetDisabled(false);
            }
        }
        disableBtn.SetDisabled(true);
    }
}
