// PnlSound: manage the controls in the Sound panel in Options. Note volume controls are handled by PnlVolume.cs
using Godot;
using System;
using System.Collections.Generic;

public class PnlSound : Panel, IPnlOptions
{

    public override void _Ready()
    {
    }

    public void Init()
    {

    }

    // Update the sound volume settings and save settings on exit. Currently only saving volume settings.
    public void Exit()
    {
        GameSettings.Instance.SaveToJSON();
    }

}
