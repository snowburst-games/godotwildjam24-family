// PnlControls: manage the controls in the Controls panel in Options

using Godot;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class PnlControls : Panel, IPnlOptions
{
	// Reference to the ID of the control btn being changed. -1 means none are being changed.
	private int changingBtn = -1;
	// input action dict to match ids with inputactions  
	private Dictionary<GameSettings.Controls, Button> inputButtons;

	private Dictionary<int, GameSettings.Controls> controlsDict = new Dictionary<int, GameSettings.Controls>()
	{
		{0, GameSettings.Controls.P1Left},
		{1, GameSettings.Controls.P1Right},
		{2, GameSettings.Controls.P1Up},
		{3, GameSettings.Controls.P1Down},
		{4, GameSettings.Controls.P1Attack},
		{5, GameSettings.Controls.P1Pause},
		{6, GameSettings.Controls.P2Left},
		{7, GameSettings.Controls.P2Right},
		{8, GameSettings.Controls.P2Up},
		{9, GameSettings.Controls.P2Down},
		{10, GameSettings.Controls.P2Attack},
		{11, GameSettings.Controls.P2Pause},
		{12, GameSettings.Controls.P1UShields},
		{13, GameSettings.Controls.P1UWeapons},
		{14, GameSettings.Controls.P1USpeed},
		{15, GameSettings.Controls.P2UShields},
		{16, GameSettings.Controls.P2UWeapons},
		{17, GameSettings.Controls.P2USpeed}
	};

	Label lblError;

	public override void _Ready()
	{   
		lblError = (Label) GetNode("LblError");
		lblError.Hide();


		// Assign our buttons
		inputButtons = new Dictionary<GameSettings.Controls, Button>()
			{
				{GameSettings.Controls.P1Left, (Button)GetNode("PnlPlayer1/BtnLeft")},
				{GameSettings.Controls.P1Right, (Button)GetNode("PnlPlayer1/BtnRight")},
				{GameSettings.Controls.P1Up, (Button)GetNode("PnlPlayer1/BtnUp")},
				{GameSettings.Controls.P1Down, (Button)GetNode("PnlPlayer1/BtnDown")},
				{GameSettings.Controls.P1Attack, (Button)GetNode("PnlPlayer1/BtnAttack")},
				{GameSettings.Controls.P2Left, (Button)GetNode("PnlPlayer2/BtnLeft")},
				{GameSettings.Controls.P2Right, (Button)GetNode("PnlPlayer2/BtnRight")},
				{GameSettings.Controls.P2Up, (Button)GetNode("PnlPlayer2/BtnUp")},
				{GameSettings.Controls.P2Down, (Button)GetNode("PnlPlayer2/BtnDown")},
				{GameSettings.Controls.P2Attack, (Button)GetNode("PnlPlayer2/BtnAttack")},
				{GameSettings.Controls.P1Pause, (Button)GetNode("PnlPlayer1/BtnPause")},
				{GameSettings.Controls.P2Pause, (Button)GetNode("PnlPlayer2/BtnPause")},
				{GameSettings.Controls.P1UShields, (Button)GetNode("PnlPlayer1/BtnShields")},
				{GameSettings.Controls.P1UWeapons, (Button)GetNode("PnlPlayer1/BtnWeap")},
				{GameSettings.Controls.P1USpeed, (Button)GetNode("PnlPlayer1/BtnSpeed")},
				{GameSettings.Controls.P2UShields, (Button)GetNode("PnlPlayer2/BtnShields")},
				{GameSettings.Controls.P2UWeapons, (Button)GetNode("PnlPlayer2/BtnWeap")},
				{GameSettings.Controls.P2USpeed, (Button)GetNode("PnlPlayer2/BtnSpeed")}
			};
		
		foreach(KeyValuePair<GameSettings.Controls, Button> kv in inputButtons)
		{
			SetButtonText(kv.Value, GameSettings.Instance.InputEvents[kv.Key]);
		}

		// Disable input to start with
		SetProcessInput(false);
	}


	// Set our button label text
	// This figures out what type of input is then sets the text. It shares a lot of code with game settings so we should
	// make common methods (DRY)
	private void SetButtonText(Button btn, string evStr)
	{
		string evType = "";
		int index = evStr.IndexOf(" ");
		if (index > 0)
		{
			evType = evStr.Substring(0, index);
		}

		if (evType == "InputEventJoypadMotion")
		{
			MatchCollection evData = Regex.Matches(evStr, @"-?\d+(\.\d+?)?", RegexOptions.Compiled);
			int axis = int.Parse(evData[0].Value);
			float axisValue = float.Parse(evData[1].Value);

			
			if (axisValue < 0)
			{
				axisValue = -1;
			}
			else
			{
				axisValue = 1;
			}

			btn.Text = "Axis " + axis + ", " + axisValue;
		}
		else if (evType == "InputEventJoypadButton")
		{
			MatchCollection evData = Regex.Matches(evStr, @"-?\d+", RegexOptions.Compiled);
			btn.Text = "Btn " + evData[0].Value;
		}
		else
		{
			btn.Text = evStr;
		}
	}

	public void Init()
	{
		lblError.Hide();
	}

	public void Exit()
	{
		GameSettings.Instance.SaveToJSON();
	}

	// On pressing a button ask for a new key
	private void _on_BtnInput_pressed(int btnID)
	{
		// Don't allow more than one key to be changed at a time
		if (changingBtn != -1)
			return;
		changingBtn = btnID;
		Button btn = inputButtons[controlsDict[btnID]];
		btn.Disabled = true;
		btn.Text = "INPUT";
		lblError.Hide();
		SetProcessInput(true);
	}

	// When we get a new key disable input again
	public override void _Input(InputEvent ev)
	{
		// GD.Print(ev.AsText());
		if (ev is InputEventKey || ev is InputEventJoypadMotion || ev is InputEventJoypadButton){
			
			SetInputAction(ev, changingBtn);
			SetProcessInput(false);
		}
	}

	// Change the specified action to the inputted key
	private void SetInputAction(InputEvent ev, int btnID)
	{
		GameSettings.Controls ctrl = controlsDict[btnID];
		Button btn = inputButtons[ctrl];
		// Enable the button after a key has been pressed
		btn.SetDisabled(false);

		// Loop through our inputs and if already taken.. swap keys
		for (int i = 0; i < GameSettings.Instance.InputActions.Count; i++){
			// Do nothing when we loop to the button we are changing
			if (i == btnID)
				continue;
			// If the control we get to is the same as the input we are trying to set our new control to..
			if (  ((InputEvent)InputMap.GetActionList(GameSettings.Instance.InputActions[controlsDict[i]])[0]).AsText() == ev.AsText()){

				lblError.Visible = true;
				// Then set the control we get to to our new control's current(old) key)
				// if (InputMap.GetActionList(GameSettings.Instance.InputActions[controlsDict[btnID]])[0] is InputEventKey ||
				//     InputMap.GetActionList(GameSettings.Instance.InputActions[controlsDict[btnID]])[0] is InputEventJoypadMotion )
				// {
				InputEvent oldEv = (InputEvent)InputMap.GetActionList(GameSettings.Instance.InputActions[controlsDict[btnID]])[0];
				SetNewInput(controlsDict[i], oldEv);
				lblError.Text = string.Format("{0} now set to {1}", GameSettings.Instance.InputActions[controlsDict[i]], oldEv.AsText());
				// }
				// //This should never come up.. it's only if an existing key is not an InputEventKey
				// else
				// {
				// 	lblError.Text = "The old key is not an input event key!";
				// 	return;
				// }
			}                
		}
		SetNewInput(ctrl, ev);
		changingBtn = -1;
	}

	private void SetNewInput(GameSettings.Controls ctrl, InputEvent ev)
	{

		Button btn = inputButtons[ctrl];
		// Get the action we clicked on
		string action = GameSettings.Instance.InputActions[ctrl];

		// Update our game settings
		GameSettings.Instance.UpdateInputEvent(ctrl, ev);
		
		// And set the label to the new key
		InputEvent inputString = (InputEvent) InputMap.GetActionList(action)[0];
		// btn.SetText(inputString.AsText());
		SetButtonText(btn, ev.AsText());

	}
}
