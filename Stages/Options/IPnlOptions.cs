// IPnlOptions: interface to guarantee methods in panels used in StageOptions
public interface IPnlOptions
{
    void Init();
    void Exit();
}
