// PnlVolume: simple script handling volume controls in the volume panel in the Sound tab in Options.
using Godot;
using System;
using System.Collections.Generic;

public class PnlVolume : Panel
{
	private Dictionary<string, HSlider> busSliderDict;

	private AudioStreamPlayer soundPlayer;

	private Dictionary<int, string> signalBusDict = new Dictionary<int, string>()
	{
		{0, "Voice"},
		{1, "Music"},
		{2, "Effects"},
		{3, "Master"}
	};

	private Dictionary<string, AudioData.SoundBus> stringBusDict = new Dictionary<string, AudioData.SoundBus>()
	{
		{"Voice", AudioData.SoundBus.Voice},
		{"Music", AudioData.SoundBus.Music},
		{"Effects", AudioData.SoundBus.Effects}
	};

	private Dictionary<string, AudioData.TestSounds> testSoundsStringsDict = new Dictionary<string, AudioData.TestSounds>()
	{
		{"Voice", AudioData.TestSounds.Voice},
		{"Music", AudioData.TestSounds.Music},
		{"Effects", AudioData.TestSounds.Effects}
	};

	private Dictionary<AudioData.TestSounds, AudioStream> testSounds;

	private bool playTestSounds = false;

	public override void _Ready()
	{
		soundPlayer = (AudioStreamPlayer)GetNode("SoundPlayer");

		// Load test sounds
		testSounds = AudioData.LoadedSounds<AudioData.TestSounds>(AudioData.TestSoundPaths);

		busSliderDict = new Dictionary<string, HSlider>() {
			{"Voice", (HSlider)GetNode("HSlVoice")},
			{"Effects", (HSlider)GetNode("HSlEffects")},
			{"Music", (HSlider)GetNode("HSlMusic")},
			{"Master", (HSlider)GetNode("HSlMaster")}            
		};

		foreach (KeyValuePair<string, HSlider> kv in busSliderDict)
		{
			kv.Value.SetValue(GameSettings.Instance.SoundVolume[kv.Key]);
		}
		playTestSounds = true;
	}
	
	private void _on_HSlSound_value_changed(float value, int bus) // 0 voice 1 music 2 effects 3 master
	{
		string busString = signalBusDict[bus];
		GameSettings.Instance.UpdateSoundVolumeSingle(busString, value);
		if (! playTestSounds || bus == 3) // Don't play special effect from master as it is misleading (varies for each bus)
			return;
		AudioStream testStream = testSounds[testSoundsStringsDict[busString]];
		AudioHandler.PlaySound(soundPlayer, testStream, stringBusDict[busString]);
	}
}


		// btnExitToLobby = (BaseButton) this.stageGame.GetNode("BtnExitToLobby"); // Show button as relevant to networked game
		// btnExitToLobby.Connect("pressed", this, nameof(OnExitToLobbyPressed));
		// btnExitToLobby.Show();
