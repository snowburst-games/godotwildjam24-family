// PnlPlayer: manage the controls in the Player panel in Options
using Godot;
using System;
using System.Collections.Generic;

public class PnlPlayer : Panel, IPnlOptions
{

    private Label lblError;
    private const int MAX_NAME_LENGTH = 10;
    private Dictionary<int, Dictionary<string, object>> playerNameDict;

    public override void _Ready()
    {
        lblError = (Label) GetNode("LblError");

        // Dict with player name data - nodes and reference to name in game settings singleton
        playerNameDict = new Dictionary<int, Dictionary<string, object>>()
        {
            {1, new Dictionary<string, object>()
                {
                    {"LineEdit", GetNode("PnlPlayer1/LEdName")}
                }
            },
            {2, new Dictionary<string, object>()
                {
                    {"LineEdit", GetNode("PnlPlayer2/LEdName")}
                }
            }
        };

        // Probably overkill but scales better if we add more players. For each player set the lineedit text to the player name to begin with.
        foreach (int player in playerNameDict.Keys){
            foreach (object le in playerNameDict[player].Values){
                if (! (le is LineEdit ledit))
                    continue;
                ledit.SetText(GameSettings.Instance.PlayerNames[player]);
            }
        }
    }

    public void Init() // Should be called when switching to the player panel
    {
        lblError.Hide();
    }

    private void OnPlayerNameLineEdit(string text, int player)
    {
        // Make reference to name and line edit for the player
        LineEdit lineEdit = (LineEdit)playerNameDict[player]["LineEdit"];
        // If the entered text does not fill our desired parameters (we just check for length for now) then show an error message..
        // .. and set line edit back to the current name
        if (! BaseProject.Utils.String.IsValidLength(text,MAX_NAME_LENGTH)){
            LineEditError(lineEdit, "Maximum 10 characters!", player);
            return;
        }
        // Show error if desired name in use.
        foreach (KeyValuePair<int, string> kv in GameSettings.Instance.PlayerNames)
        {
            if (kv.Key == player)
                continue;
            if (kv.Value == text)
            {
                LineEditError(lineEdit, "Name already in use!", player);
                return;
            }
        }
        // Otherwise set the player name in settings
        GameSettings.Instance.UpdatePlayerName(player, lineEdit.Text);
    }

    private void LineEditError(LineEdit lineEdit, string message, int player)
    {
        lblError.Show();
        lblError.SetText(message);
        lineEdit.SetText(GameSettings.Instance.PlayerNames[player]);
    }

    // When either of the line edits focus exit
	private void _on_LEdName_focus_exited()
	{
	    // lblError.Hide();
	}

    private void _on_LEdName_text_entered(string text, int player) // 1 = player 1, 2 = player 2
    {
        OnPlayerNameLineEdit(text, player);
    }
    
    // Method for each line edit. 
    // Another way to do this is to make current text, current player member variables rather than having separate methods.
    private void _on_LEdName2_focus_exited()
    {
        LineEdit lineEdit = (LineEdit)playerNameDict[2]["LineEdit"];
        OnPlayerNameLineEdit(lineEdit.GetText(), 2);
    }

    private void _on_LEdName1_focus_exited()
    {
        LineEdit lineEdit = (LineEdit)playerNameDict[1]["LineEdit"];
        OnPlayerNameLineEdit(lineEdit.GetText(), 1);
    }

    private void _on_LEdName_text_changed(String new_text)
    {
        lblError.Hide();
    }

    public void Exit()
    {
        GameSettings.Instance.SaveToJSON();
    }
}




