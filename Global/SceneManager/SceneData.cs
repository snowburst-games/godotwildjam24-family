// Scene data: contains references to all Stage scenes within the game - for simplicity so we don't have to update path in each script.

using System;

public class SceneData
{
    public enum Stage
    {
        Lobby,
        Game,
        MainMenu,
        Scores,
        Options,
        IntensiveTest,
        Multiplayer,
        Lobbies
    }

    public enum Scene
    {
        NetState,
        Lobby,
        Player,
        Creature
    }

    public readonly System.Collections.Generic.Dictionary<Stage,string> Stages 
        = new System.Collections.Generic.Dictionary<Stage, string>()
    {
        {Stage.Lobby, "res://Stages/Lobby/StageLobby.tscn"},
        {Stage.Game, "res://Stages/Game/StageGame.tscn"},
        {Stage.MainMenu, "res://Stages/MainMenu/StageMainMenu.tscn"},
        {Stage.Scores, "res://Stages/Scores/StageScores.tscn"},
        {Stage.Options, "res://Stages/Options/StageOptions.tscn"},
        {Stage.IntensiveTest, "res://Stages/IntensiveTest/StageIntensiveTest.tscn"},
        {Stage.Multiplayer, "res://Stages/Multiplayer/StageMultiplayer.tscn"},
        {Stage.Lobbies, "res://Stages/Lobbies/StageLobbies.tscn"}        
    };

    public static System.Collections.Generic.Dictionary<Scene,string> Scenes 
        = new System.Collections.Generic.Dictionary<Scene, string>()
    {
        {Scene.NetState, "res://Global/NetState/NetState.tscn"},
        {Scene.Lobby, "res://Utils/Network/Lobby/Lobby.tscn"},
        {Scene.Player, "res://Entities/Characters/Player/Player.tscn"},
        {Scene.Creature, "res://Entities/Creature/Creature.tscn"}
    };
}
